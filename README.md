Выполненные задачи:
- Установлена Ubuntu Desktop. Попытка установить Ubuntu server не увенчалась успехом: 
    OS устанавливается, но не получается зайти по стандартному логину/паролю ubuntu/ubuntu
    Вероятно это связано с проблемами с настройкой WiFi адаптера
    Не помогла даже "Шаг 2. Добавьте поддержку Wi-Fi на сервер Ubuntu" инструкции https://websetnet.net/ru/how-to-install-ubuntu-server-on-a-raspberry-pi/
  
- Ubuntu была обновлена (sudo apt-get update) и перезагружена.
- Написаны скрипты для измерения температуры и обнаружения устройств, подключенных к USB портам
- Написан bash script scheduler.sh, в котором прописан запуск temperature_reporter.py
- Создана задача cron (через sudo crontab -e) с автозапуском скрипта scheduler.sh
- Установлен ssh-server (sudo apt-get install openssh-server). Raspberry поддерживает соединение по ssh.
    Следует установить RSA ключи, добавить ключи в trusted и отключить вход по паролю (оставить только RSA)
- Код для формирования JSON и отправки его на UART порт написан, но не протестирован за неимением приемника.
