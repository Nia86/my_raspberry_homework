#!/usr/bin/python3

import os
from time import sleep

import json
import serial
from pprint import pprint


def temperature_of_raspberry_pi():
    cpu_temp = os.popen("vcgencmd measure_temp").readline()
    return cpu_temp.replace("temp=", "")


if __name__ == "__main__":
    while True:
        sleep(1)
        ser = serial.Serial("COM3", baudrate=9600,
                            timeout=2.5,
                            parity=serial.PARITY_NONE,
                            bytesize=serial.EIGHTBITS,
                            stopbits=serial.STOPBITS_ONE
                            )

        data = {'temperature': temperature_of_raspberry_pi(), "operation": "sequence"}
        data = json.dumps(data)
        print(data)
        if ser.isOpen():
            ser.write(data.encode("ascii"))
            ser.flush()
            try:
                incoming = ser.readline().decode("utf-8")
                print(incoming)
            except Exception as e:
                print(e)
                pass
            ser.close()
        else:
            print("opening error")

